Imperva

How to Run the Code:

Training the Model:
1. Execute the train_model.py file to train the model. This file generates an AUC score of 0.99997.

2. Making Predictions on New Data:
install docker (https://www.docker.com/products/docker-desktop).
We need to Pull the Docker image with the following commands in the cmd:
1. docker login
2. docker pull shatssss/imperva:latest
3. Run the Docker image: docker run -p 5000:5000 shatssss/imperva:latest

The service is now online, enabling message sending. You have two options:
1. Manually send a request by making a POST call to http://localhost:5000/predict, providing the 'file_path' argument with the path to the file.
2. Execute the [request.py](request.py) script to send a request to the container (default file is hometest_dataset.json). This script sends the file name for prediction. Remember to adjust the file path using the file_path_to_predict variable in the script.