import pickle

import pandas as pd
import xgboost as xgb
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split

from helper import jsons_to_vectors

TRAIN_DATA_PATH = 'resources/train_dataset.json'


def main():
    print(f'starting creating features')
    # read data and create features vector
    train_features_vector = jsons_to_vectors(TRAIN_DATA_PATH)
    train_df = pd.DataFrame(train_features_vector[0], columns=pd.MultiIndex.from_tuples(train_features_vector[1]))
    print(f'features creation done!')

    print(f'starting creating labels')
    train_data_df = pd.read_json(TRAIN_DATA_PATH)
    print(f'finish reading the data')
    labels = train_data_df.apply(lambda row: 1 if row[0]['user'] == row[1]['user'] else 0, axis=1)
    print(f'labels creation done!')

    # split data to train and test
    X_train, X_val, y_train, y_val = train_test_split(train_df, labels, test_size=0.2, random_state=42)

    # train model
    model = get_trained_model(X_train, y_train)
    model.save_model('resources/xgboost_model.model')
    print(f'model saved!')

    # calc model auc
    auc = cal_model_auc(model, X_val, y_val)
    print(f'Auc: {auc}')

    # save features to a file
    with open('resources/features.pkl', 'wb') as f:
        pickle.dump(train_features_vector[1], f)


def cal_model_auc(model, X_val, y_val):
    y_probs = model.predict_proba(X_val)[:, 1]
    auc = roc_auc_score(y_val, y_probs)
    return auc


def get_trained_model(X_train, y_train):
    model = xgb.XGBClassifier()
    model.fit(X_train, y_train)
    return model


if __name__ == '__main__':
    main()
