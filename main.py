import pickle

import pandas as pd
import xgboost as xgb
from flask import Flask, request, jsonify

from helper import jsons_to_vectors

app = Flask(__name__)


@app.route('/predict', methods=['POST'])
def predict():
    print(f'Starting prediction')
    # Get the file path from the request data
    file_path = request.json['file_path']

    # create features on the test data
    test_features_vector = jsons_to_vectors(file_path)
    test_df = pd.DataFrame(test_features_vector[0], columns=pd.MultiIndex.from_tuples(test_features_vector[1]))

    # load features from training stage in order to filter unseen features in the test data
    # xgboost knows how to handle missing columns
    with open('resources/features.pkl', 'rb') as f:
        train_features = pickle.load(f)
        train_features = pd.MultiIndex.from_tuples(train_features)
    test_df = test_df.loc[:, test_df.columns.isin(train_features)]

    # Load trained model
    booster = xgb.Booster(model_file='resources/xgboost_model.model')

    # make predictions
    # convert probabilities to "yes"/"no"
    final_predictions = calc_predictions(booster, test_df)
    return jsonify(final_predictions)


def calc_predictions(booster, test_df):
    dmatrix = xgb.DMatrix(test_df)
    predictions = booster.predict(dmatrix)
    threshold = 0.5
    binary_predictions = (predictions >= threshold).astype(int)
    final_predictions = ["yes" if prediction == 1 else "no" for prediction in binary_predictions]
    return final_predictions


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
