import json

"""
helper.py
optional functionality for loading the data and transforming it to a feature vector.
The relevant entry point is the function "jsons to vectors". Other functions are used
internally by this function.
"""


def jsons_to_vectors(dataset):
    """
    input: location of pairs of device fingerprints dataset (user characteristics)
    output:
        1) list of vectors corresponding to the dataset
        2) feature index of the vectors
    Cell i is valued 1 if feature i value (according to the feature index) is identical between the two
    matched user characteristics and 0 otherwise.
    """
    with open(dataset) as f:
        json_obj = json.load(f)

    try:
        all_attributes = []  # this is the index for the feature vector
        ATTRIBUTES_FILTER_LIST = ['user']
        _get_all_attributes(json_obj[0][0], [], all_attributes, filter_list=ATTRIBUTES_FILTER_LIST)
        feature_vectors_list = []
        for pair in json_obj:
            feature_vector = _create_feature_vector(pair, all_attributes)
            feature_vectors_list.append(feature_vector)
        return feature_vectors_list, all_attributes
    except Exception as e:
        print(e)
        return False


def _create_feature_vector(fingerprint_pair, feature_index):
    fingerprint1, fingerprint2 = fingerprint_pair
    feature_vector = []
    for feature_hierarchy in feature_index:
        feature_vector.append(_compare_feature_in_fingerprints(fingerprint1, fingerprint2, feature_hierarchy))
    return feature_vector


def _compare_feature_in_fingerprints(fingerprint1, fingerprint2, feature_hierarchy):
    different_features_label = 0
    same_features_label = 1
    cur_node1, cur_node2 = fingerprint1, fingerprint2
    for attribute in feature_hierarchy:
        try:
            cur_node1 = cur_node1[attribute]
            cur_node2 = cur_node2[attribute]
        except:
            return different_features_label
    if cur_node1 == cur_node2:
        return same_features_label
    else:
        return different_features_label


def _get_all_attributes(previous_node, attribute_hierarchy_list, all_attributes, print_flag=False, filter_list=None):
    for attribute in previous_node:
        if isinstance(previous_node[attribute], dict):
            attribute_hierarchy_list.append(attribute)
            _get_all_attributes(previous_node[attribute], attribute_hierarchy_list, all_attributes, print_flag,
                                filter_list)
            del attribute_hierarchy_list[-1]
        else:
            attribute_hierarchy_list.append(attribute)
            if print_flag:
                print(attribute_hierarchy_list)
            if filter_list is not None and not attribute_hierarchy_list[0] in filter_list:
                all_attributes.append(tuple(attribute_hierarchy_list))
            del attribute_hierarchy_list[-1]
